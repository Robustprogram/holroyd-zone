import React from 'react';


import Legends from '../assets/legends.json';
import './ControlPanel.scss';


class ControlPanel extends React.Component {
  static defaultProps = {
    opacity: 0.1
  }

  updateOpacity = (e) => {
    this.props.updateOpacity(e.target.value);
  }

  render() {
    const {opacity} = this.props;
    let colourBoxElm = [];

    for (const colour of Legends.legends) {
      colourBoxElm.push(
        <li key={colour.id} className="colour-item">
          <span className="colour" style={{backgroundColor: colour.colour}}>
            {colour.id}
          </span> - {colour.name}
        </li>
      );
    }

    return (
      <div className="uk-position-small uk-position-absolute uk-position-center-left uk-position-z-index uk-background-default uk-padding-small uk-width-1-4">
        <div className="uk-margin">
          <label className="uk-form-label">Colour Legend</label>
          <ul id="colour-box" className="uk-list uk-list-striped">{colourBoxElm}</ul>
        </div>
        <div className="uk-margin">
          <label className="uk-form-label" htmlFor="form-stacked-select">Overlay Visibility</label>
          <div className="uk-form-controls">
            <input
              id="form-stacked-select" className="uk-range"
              type="range"
              value={opacity} min="0" max="1" step="0.1"
              onChange={this.updateOpacity}
            />
          </div>
        </div>
      </div>
    );
  }
};

export default ControlPanel;
