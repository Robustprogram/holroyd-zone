import React from 'react';
import mapboxgl from 'mapbox-gl';
import MapboxGeocoder from '@mapbox/mapbox-gl-geocoder';
import ControlPanel from './ControlPanel';
import ClipLoader from "react-spinners/ClipLoader";


import OverlayImage from '../assets/collected-trans.png';
import './App.scss';


mapboxgl.accessToken = process.env.REACT_APP_MAPBOX_API_KEY;


class App extends React.Component {
  static defaultProps = {
    lng: 150.9883,
    lat: -33.8021,
    zoom: 12,
  }

  state = {
    overlayCoords: [
      [150.91173493099997, -33.783292694],
      [151.011349018, -33.785016165500006],
      [151.00904901799997, -33.8753075246],
      [150.909634931, -33.8738075246],
    ],
    overlayOpacity: 0.25,
    mapObj: undefined,
    loaded: false,
  }

  componentDidMount() {
    const {lng, lat, zoom} = this.props;
    const {overlayCoords, overlayOpacity} = this.state;

    const map = new mapboxgl.Map({
      container: this.mapContainer,
      style: 'mapbox://styles/mapbox/streets-v11',
      center: [lng, lat],
      zoom: zoom
    });

    map.addControl(
      new MapboxGeocoder({
        accessToken: process.env.REACT_APP_MAPBOX_API_KEY,
        mapboxgl: mapboxgl,
      }),
      'top-left'
    );

    map.on('load', () => {

      map.addSource('imageTest', {
        'type': 'image',
        'url': OverlayImage,
        'coordinates': overlayCoords,
      });
      map.addLayer({
        'id': 'overlay',
        'source': 'imageTest',
        'type': 'raster',
      });
      map.setPaintProperty(
        'overlay',
        'raster-opacity',
        overlayOpacity
      );

      this.setState({loaded: true});
    });

    this.setState({mapObj: map});
  }

  updateOpacity = (value) => {
    const {mapObj} = this.state;

    console.log(value);

    mapObj.setPaintProperty(
      'overlay',
      'raster-opacity',
      parseFloat(value),
    );

    this.setState({overlayOpacity: value});
  }

  render() {
    const {overlayOpacity, loaded} = this.state;

    return (
      <div className="App">
        <div id="loader" className="uk-overlay-default uk-position-cover" hidden={loaded}>
          <div className="uk-position-center">
            <ClipLoader /><span className="separator"/> Loading Map
          </div>
        </div>
        <ControlPanel opacity={overlayOpacity} updateOpacity={this.updateOpacity}/>
        <div ref={el => this.mapContainer = el} className="mapContainer"/>
      </div>
    );
  }
}

export default App;
